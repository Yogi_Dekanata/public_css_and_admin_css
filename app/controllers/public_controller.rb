class PublicController < ApplicationController
  caches_page :index

  layout 'public'

  def index
  end

end
